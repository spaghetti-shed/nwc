/******************************************************************************
 * tcppollserver.c: A TCP polling server based on Beej's examples.
 *
 * Wed Nov  8 22:16:18 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This code is heavily inspired by Beej's public domain example code in his
 * awesome Guide to Network Programming at https://beej.us/guide/bgnet/
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-08 Initial creation. Extracted from nwc.c.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include "timber.h"
#include "mal.h"
#include "nwc.h"
#include "pfdtable/pfdtable.h"

#define NO_OF_OBJECTS   1024


#define DEFAULT_TCPIP_FDS   8

#define MAX_CLIENTS     3
#define NO_OF_FDS       (MAX_CLIENTS + 1)
#define DEFAULT_PORT    "9034"

int main(int argc, char *argv[])
{
    int32_t nspare;
    int     lfd;      /* listener file descriptor */

    pfdtable_t *pfdtable;

    TDR_TIME_INIT();
    MAL_OPEN(NO_OF_OBJECTS);

    TDR_DIAG_INFO("TCP client/server polling demo.\n");

    if (NULL == (pfdtable = pfdtable_create(NO_OF_FDS)))
    {
        TDR_DIAG_ERROR("Failed to create pfdtable.\n");
        exit(EXIT_FAILURE);
    }

    if (0 != nwc_listener_tcp_socket_get(DEFAULT_PORT, MAX_CLIENTS, &lfd))
    {
        TDR_DIAG_ERROR("Failed to create listener on socket %s.\n", DEFAULT_PORT);
        exit(EXIT_FAILURE);
    }

    /*
     * Add the listener file descriptor to our table.
     */
    pfdtable_add_fd(pfdtable, lfd);
    pfdtable_set_listener(pfdtable, lfd);

    /*
     * Go polling.
     */
    TDR_DIAG_INFO("Polling.\n");

    while (1)
    {
        nwc_tcp_polling_loop(pfdtable);
    }

    pfdtable_destroy(&pfdtable);

    if (0 != (nspare = MAL_CLOSE()))
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }

    return 0;
}

