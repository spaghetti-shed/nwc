Networking Wrappers in C
========================

Networking Wrappers in C (nwc) is a libraray of C wrapper functions inspired by
Brian "Beej Jorgensen" Hall's example code from his "Beej's Guide to Network
Programming Using Internet Sockets."

https://beej.us/guide/bgnet/

Copyright (C) 2023 by Iain Nicholson. iain.j.nicholson@gmail.com

  This file is part of nwc (Networking Wrappers in C).
 
  nwc is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published
  by the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.
 
  nwc is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with nwc; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  USA

Introduction
------------

This library of code is an attempt to develop a high-level API for network
programming starting with Beej's guide and example code for advice and
inspiration.

Beej has very generously put his examples out in the Public Domain. This is not
an attempt to rip off Beej and claim his work as my own. This is an attempt by
myself of using his examples to create a simpler API for hacking together
networking applications and utilties in C as quickly and cheaply as possible.

You will notice that the code is strongly reminiscent of his examples but I have
rewritten things in my own crazy style and attempted to identify places where
behaviour should be encapsulated.

This was done in a hurry. It was not well tested.

Compiling
---------

Nwc depends on the following libraries from Captain Spaghetti's Code Shed:
  timber
  mal
  cutl

Compile and install them first (make && make install).
Then compile nwc.

Usage
-----

./nwc

In other widows, connect using telnet to the machine running nwc on port 9034.
Type in one telnet session to see the text echoed in the others.


TO-DO:
* Lots more.
* Better unit tests.
* Automated regression tests.

