/******************************************************************************
 * nwc: Networking Wrappers in C (test).
 *
 * Sat Nov  4 15:44:08 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This code is heavily inspired by Beej's public domain example code in his
 * awesome Guide to Network Programming at https://beej.us/guide/bgnet/
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "nwc.h"
#include "cutl.h"
#include "mal.h"

void test_nwc_example(void);
cutl_test_case_t tests[] =
{
    { "nwc_example", test_nwc_example },
};

void nwc_test_case_setup(void);
void nwc_test_case_teardown(void);

#define NO_OF_OBJECTS   1024

void nwc_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
    cutl_stdlib_test_stubs_reset();
}

void nwc_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();
    if (nspare > 0)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }
}

int main(int argc, char *argv[])
{
    printf("nwc test.\n");

    cutl_set_test_lib("nwc", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(nwc_test_case_setup);
    cutl_set_cutl_test_teardown(nwc_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

void test_nwc_example(void)
{
    TEST_INT32_ZERO("zero", 0);
}


