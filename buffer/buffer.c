/******************************************************************************
 * buffer.c: A dynamically-allocated buffer object.
 *
 * Sat Dec  9 20:12:27 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-12-09 Initial creation.
 * 2023-12-10 Add mutex.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "timber.h"
#include "mal.h"
#include "buffer.h"

buffer_t *buffer_allocate(void)
{
    buffer_t *buffer;

    buffer = MAL_MALLOC(sizeof(buffer_t));

    return buffer;
}

void buffer_init(buffer_t *buffer)
{
    buffer_set_data(buffer, 0);
    buffer_set_data_size(buffer, 0);
    buffer_set_data_used(buffer, 0);
}

/*
 * Internal do nothing default nofity callback function.
 */
int32_t default_notify(buffer_t *buffer)
{
    return 0;
}

buffer_t *buffer_create(size_t size)
{
    buffer_t *buffer;

    buffer = buffer_allocate();
    if(NULL == buffer)
    {
        TDR_DIAG_ERROR("buffer_allocate() failed.\n");
        exit(EXIT_FAILURE);
    }
    else if (NULL == (buffer->data = MAL_MALLOC(size)))
    {
        MAL_FREE(buffer);
        buffer = NULL;
    }
    else
    {
        pthread_mutexattr_init(&buffer->mutexattr);
        pthread_mutex_init(&buffer->mutex, &buffer->mutexattr);
        buffer->data_size = size;
        buffer->data_used = 0;
        buffer->notify    = default_notify;
    }

    return buffer;
}

void buffer_destroy(buffer_t **buffer)
{
    if(NULL == buffer)
    {
        TDR_DIAG_ERROR("buffer_destroy(): NULL pointer received.\n");
    }
    else
    {
        pthread_mutexattr_destroy(&(*buffer)->mutexattr);
        pthread_mutex_destroy(&(*buffer)->mutex);
        if (NULL != (*buffer)->data)
        {
            MAL_FREE((*buffer)->data);
            (*buffer)->data = NULL;
        }
        MAL_FREE(*buffer);
        *buffer = NULL;
    }
}

uint8_t * buffer_get_data(buffer_t *buffer)
{
    return buffer->data;
}


void buffer_set_data(buffer_t *buffer, uint8_t * data)
{
    buffer->data = data;
}

size_t buffer_get_data_size(buffer_t *buffer)
{
    return buffer->data_size;
}


void buffer_set_data_size(buffer_t *buffer, size_t data_size)
{
    buffer->data_size = data_size;
}

size_t buffer_get_data_used(buffer_t *buffer)
{
    return buffer->data_used;
}


void buffer_set_data_used(buffer_t *buffer, size_t data_used)
{
    buffer->data_used = data_used;
}

int32_t buffer_mutex_lock_with_timeout(buffer_t *buffer)
{
    int32_t retcode = -1;
    int32_t tries = 0;

    if (NULL == buffer)
    {
    }
    else
    {
        while ((tries < MUTEX_LOCK_TRIES) && (0 != (retcode = pthread_mutex_trylock(&buffer->mutex))))
        {
            tries++;
            usleep(MUTEX_LOCK_PERIOD_US);
        }

        if (MUTEX_LOCK_TRIES == tries)
        {
            /* One last try. */
            retcode = pthread_mutex_trylock(&buffer->mutex);
        }

        if (0 != retcode)
        {
            TDR_DIAG_ERROR("Failed to lock mutex for buffer %p\n", buffer);
        }
    }

    return retcode;
}

int32_t buffer_mutex_unlock(buffer_t *buffer)
{
    int32_t retcode = -1;

    if (NULL == buffer)
    {
    }
    else
    {
        retcode = pthread_mutex_unlock(&buffer->mutex);

        if (0 != retcode)
        {
            TDR_DIAG_ERROR("Failed to unlock mutex for buffer %p\n", buffer);
        }
    }

    return retcode;
}

int32_t  buffer_read_data(buffer_t *buffer, void *dest, size_t dest_size, size_t *dest_used)
{
    int32_t retcode = -1;
    
    if (NULL == buffer)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (NULL == dest_used)
    {
    }
    else if (0 != buffer_mutex_lock_with_timeout(buffer))
    {
    }
    else
    {
        if (0 == buffer->data_used)
        {
            /* Nothing to do.  Why were we called? */
        }
        else if (dest_size < buffer->data_used)
        {
            /* Output buffer too small. */
        }
        else
        {
            memcpy(dest, buffer->data, buffer->data_used);
            *dest_used = buffer->data_used;
            buffer->data_used = 0;
            retcode = 0;
        }

        if (0 != buffer_mutex_unlock(buffer))
        {
            /* This really shouldn't happen. */
            retcode = -1;
        }

    }

    return retcode;
}

int32_t  buffer_write_data(buffer_t *buffer, void *source, size_t source_size)
{
    int32_t retcode = -1;
    
    if (NULL == buffer)
    {
    }
    else if (NULL == source)
    {
    }
    else if (0 == source_size)
    {
    }
    else if (0 != buffer_mutex_lock_with_timeout(buffer))
    {
    }
    else
    {
        if (0 != buffer->data_used)
        {
            TDR_DIAG_ERROR("Buffer %p already contains %zd bytes of data.\n", buffer, buffer->data_used);
        }
        else if (buffer->data_size < source_size)
        {
            /* Buffer too small. */
        }
        else
        {
            memcpy(buffer->data, source, source_size);
            buffer->data_used = source_size;
            retcode = 0;
        }

        if (0 != buffer_mutex_unlock(buffer))
        {
            /* This really shouldn't happen. */
            retcode = -1;
        }

    }

    return retcode;
}



