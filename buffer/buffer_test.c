/******************************************************************************
 * buffer (test)
 * Sat Dec  9 20:12:27 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. All rights reserved.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "buffer.h"
#include "cutl.h"
#include "mal.h"

void test_buffer_allocation(void);
void test_buffer_accessors(void);

cutl_test_case_t tests[] =
{
    { "buffer_allocation", test_buffer_allocation },
    { "buffer_accessors",  test_buffer_accessors  },
};

void buffer_test_case_setup(void);
void buffer_test_case_teardown(void);

#define NO_OF_OBJECTS   1024

void buffer_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
    cutl_stdlib_test_stubs_reset();
}

void buffer_test_case_teardown(void)
{
    int32_t nspare;

    if (0 != (nspare = MAL_CLOSE()))
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }
}

int main(int argc, char *argv[])
{
    printf("buffer test.\n");

    cutl_set_test_lib("buffer", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(buffer_test_case_setup);
    cutl_set_cutl_test_teardown(buffer_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

#define BUFFER_SIZE 65536
void test_buffer_allocation(void)
{
    buffer_t *buffer = NULL;

    buffer = buffer_create(BUFFER_SIZE);
    TEST_PTR_NOT_NULL("buffer_create()", buffer);
    TEST_INT32_EQUAL("MAL_IS_VALID", 1, MAL_IS_VALID(buffer));

    TEST_SIZE_T_EQUAL("buffer_size", BUFFER_SIZE, buffer_get_data_size(buffer));

    buffer_destroy(&buffer);
    TEST_PTR_NULL("buffer_destroy()", buffer);
    TEST_INT32_EQUAL("MAL_IS_VALID", 0, MAL_IS_VALID(buffer));
}


void test_buffer_accessors(void)
{
    buffer_t buffer;

    buffer_set_data(&buffer, 0);
    assert(0 == buffer_get_data(&buffer));
    buffer_set_data_size(&buffer, 0);
    assert(0 == buffer_get_data_size(&buffer));
    buffer_set_data_used(&buffer, 0);
    assert(0 == buffer_get_data_used(&buffer));
}


