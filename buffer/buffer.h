/******************************************************************************
 * buffer.h: A dynamically-allocated buffer object (header).
 *
 * Sat Dec  9 20:12:27 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-12-09 Initial creation.
 * 2023-12-10 Add mutex and notification callback.
 ******************************************************************************/

#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#define MUTEX_LOCK_PERIOD_US    1000
#define MUTEX_LOCK_TRIES        100

#ifdef __cplusplus
extern "C"
{
#endif

struct buffer_t
{
    uint8_t *data;
    size_t data_size;
    size_t data_used;
    pthread_mutex_t mutex;
    pthread_mutexattr_t mutexattr;
    int32_t (*notify)(struct buffer_t *buffer);
};

typedef struct buffer_t buffer_t;

buffer_t *buffer_create(size_t size);
void      buffer_destroy(buffer_t **buffer);

uint8_t *buffer_get_data(buffer_t *buffer);
void     buffer_set_data(buffer_t *buffer, uint8_t * data);
size_t   buffer_get_data_size(buffer_t *buffer);
void     buffer_set_data_size(buffer_t *buffer, size_t data_size);
size_t   buffer_get_data_used(buffer_t *buffer);
void     buffer_set_data_used(buffer_t *buffer, size_t data_used);

int32_t  buffer_read_data(buffer_t *buffer, void *dest, size_t dest_size, size_t *dest_used);
int32_t  buffer_write_data(buffer_t *buffer, void *source, size_t source_size);

#ifdef __cplusplus
}
#endif

#endif /* __BUFFER_H__ */

