/******************************************************************************
 * nwc.h: Networking Wrappers in C (header).
 *
 * Sat Nov  4 15:44:08 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This code is heavily inspired by Beej's public domain example code in his
 * awesome Guide to Network Programming at https://beej.us/guide/bgnet/
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 * 2023-12-06 Add UDP functions.
 ******************************************************************************/

#ifndef __NWC_H__
#define __NWC_H__

#include <stdint.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <arpa/inet.h>

#include "pfdtable/pfdtable.h"
#include "buffer/buffer.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define TCPIPV4_MAX         65535
#define TCPIPV4_HEADER_MIN  20
#define UDPIPV4_HEADER      8
#define UDPIPV4_PAYLOAD_MAX ((TCPIPV4_MAX - TCPIPV4_HEADER_MIN) - UDPIPV4_HEADER)

int32_t nwc_hints_tcp_set(struct addrinfo *hints);
int32_t nwc_hints_udp_set(struct addrinfo *hints);
void nwc_aiu_suppress(int socket);
int32_t nwc_listener_tcp_socket_get(const char *restrict port, int backlog, int *lfd);
int32_t nwc_listener_udp_socket_get(const char *restrict port, int *udpfd);
int32_t nwc_sender_udp_socket_get(const char *server, const char *restrict port, int *udpfd, struct addrinfo **p);
void *nwc_in_addr_get(struct sockaddr *sa);
int32_t nwc_tcp_polling_loop(pfdtable_t *pfdtable);
int32_t nwc_tcp_callback_loop(pfdtable_t *pfdtable);
int32_t nwc_udp_polling_loop(int udpfd);
int32_t nwc_udp_callback_server(const char *restrict port, int32_t (*notify)(struct buffer_t *buffer));

void hexdump(uint8_t *buffer, size_t size);


#ifdef __cplusplus
}
#endif

#endif /* __NWC_H__ */

