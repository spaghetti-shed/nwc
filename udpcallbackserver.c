/******************************************************************************
 * udpcallbackserver.c: A UDP polling server based on Beej's examples running
 *                      in a background thread and using a callback function to
 *                      notify the outside world when the receive buffer
 *                      contains data.
 *
 * Sat Dec  9 20:46:56 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This code is heavily inspired by Beej's public domain example code in his
 * awesome Guide to Network Programming at https://beej.us/guide/bgnet/
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-12-09 Initial creation. Extracted from updpollserver.c.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include "timber.h"
#include "mal.h"
#include "nwc.h"
#include "buffer/buffer.h"

#define NO_OF_OBJECTS       1024

#define DEFAULT_PORT        "4950"

#define HEXDUMP_MAX_COLUMNS 16

/*
 * Horrible, nasty global variables.
 */
uint8_t receive[UDPIPV4_PAYLOAD_MAX];
size_t receive_used = 0;

int32_t udp_notify(buffer_t *buffer)
{
    int32_t retcode = -1;

    if (0 == (retcode = buffer_read_data(buffer, receive, sizeof(receive), &receive_used)))
    {
        hexdump(receive, receive_used);
    }

    return retcode;
}


int main(int argc, char *argv[])
{
    int32_t nspare;

    TDR_TIME_INIT();
    MAL_OPEN(NO_OF_OBJECTS);

    TDR_DIAG_INFO("UDP server thread with notification callbacks demo. Port %s\n", DEFAULT_PORT);

    if (0 != nwc_udp_callback_server(DEFAULT_PORT, udp_notify))
    {
        TDR_DIAG_ERROR("Failed to create UDP server on socket %s.\n", DEFAULT_PORT);
        exit(EXIT_FAILURE);
    }

    /*
     * Wait for things to happen.
     */
    TDR_DIAG_INFO("Waiting.\n");

    while (1)
    {
    }

    if (0 != (nspare = MAL_CLOSE()))
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }

    TDR_DIAG_INFO("Done.\n");

    return 0;
}

