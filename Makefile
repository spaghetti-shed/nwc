#
# Makefile for nwc: Networking Wrappers in C.
# Sat Nov  4 15:44:08 GMT 2023
#
# Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This code is heavily inspired by Beej's public domain example code in his
# awesome Guide to Network Programming at https://beej.us/guide/bgnet/
#
#  This file is part of nwc (Networking Wrappers in C).
#
#  nwc is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published
#  by the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
#  nwc is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with nwc; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
#
# Modification history:
# 2023-11-04 Initial creation.
# 2023-12-09 Add buffer.
# 2023-12-09 Add buffer mutexes and monitors. UDP callback server.
#

CC=gcc
CFLAGS=-Wall -Werror -O
CPPTESTFLAGS=-DTESTING
INCLUDES=-I/usr/local/include
LIBS=-lcutl -ltimber -lmal -lpthread

.PHONY: pfdtable
.PHONY: buffer

all: main test

test: nwc_test
	./nwc_test

main: buffer pfdtable nwc tcppollserver tcpcallback udploopserver udpcallbackserver udpsend

nwc: nwc.h nwc.c
	$(CC) $(CFLAGS) $(INCLUDES) -c nwc.c

nwc_test: nwc_test.c nwc.c nwc.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -DTESTSTUBS $(INCLUDES) -o nwc_testing.o -c nwc.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) $(INCLUDES) -c nwc_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o nwc_test nwc_test.o nwc_testing.o pfdtable/pfdtable_testing.o buffer/buffer.o $(LIBS)

tcppollserver: nwc tcppollserver.c
	$(CC) $(CFLAGS) $(INCLUDES) -c tcppollserver.c
	$(CC) $(CFLAGS) -o tcppollserver tcppollserver.o nwc.o pfdtable/pfdtable.o buffer/buffer.o $(LIBS)

tcpcallback: nwc tcpcallback.c
	$(CC) $(CFLAGS) $(INCLUDES) -c tcpcallback.c
	$(CC) $(CFLAGS) -o tcpcallback tcpcallback.o nwc.o pfdtable/pfdtable.o buffer/buffer.o $(LIBS)

udploopserver: nwc udploopserver.c
	$(CC) $(CFLAGS) $(INCLUDES) -c udploopserver.c
	$(CC) $(CFLAGS) -o udploopserver udploopserver.o nwc.o pfdtable/pfdtable.o buffer/buffer.o $(LIBS)

udpcallbackserver: nwc udpcallbackserver.c
	$(CC) $(CFLAGS) $(INCLUDES) -c udpcallbackserver.c
	$(CC) $(CFLAGS) -o udpcallbackserver udpcallbackserver.o nwc.o pfdtable/pfdtable.o buffer/buffer.o $(LIBS)

udpsend: nwc udpsend.c
	$(CC) $(CFLAGS) $(INCLUDES) -c udpsend.c
	$(CC) $(CFLAGS) -o udpsend udpsend.o nwc.o pfdtable/pfdtable.o buffer/buffer.o $(LIBS)

pfdtable:
	$(MAKE) -C pfdtable

buffer:
	$(MAKE) -C buffer

clean:
	$(MAKE) -C pfdtable clean
	$(MAKE) -C buffer clean
	rm -f *.o
	rm -f nwc_test
	rm -f nwc
	rm -f tcppollserver
	rm -f udploopserver
	rm -f udpcallbackserver
	rm -f udpsend
	rm -f tcpcallback

