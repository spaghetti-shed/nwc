/******************************************************************************
 * nwc: Networking Wrappers in C.
 *
 * Sat Nov  4 15:44:08 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This code is heavily inspired by Beej's public domain example code in his
 * awesome Guide to Network Programming at https://beej.us/guide/bgnet/
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 * 2023-12-06 Add UDP functions.
 * 2023-12-09 A UDP server in a thread.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <arpa/inet.h>


#include "timber.h"
#include "mal.h"
#include "nwc.h"
#include "pfdtable/pfdtable.h"

#define NO_OF_OBJECTS   1024

/*
 * For a TCP/IP poll() server, from Beej's guide the sequence of function
 * calls is:
 *   set hints
 *   getaddrinfo
 *   socket
 *   bind
 *   listen
 *   poll
 *     accept
 *     recv
 *
 * There must be a listener file descriptor (for control connections) and an
 * arbitrary number of send/receive file descriptors for data.
 */

#define DEFAULT_TCPIP_FDS   8
#define DRAIN_BUFFER_SIZE   256
#define RECV_FLAGS_CLEAR    0

static uint8_t drain_buffer[DRAIN_BUFFER_SIZE];

void default_receive_callback(int fd)
{
    TDR_DIAG_INFO("Got data on file descriptor %d.\n", fd);
    /*
     * What this shows is that if you don't read the data, the polling flag
     * doesn't get cleared and you go round in an infinite loop.
     */
    recv(fd, drain_buffer, sizeof(drain_buffer), RECV_FLAGS_CLEAR);
    /*
     * No, you still get the infinite loop.
     */
}

static void (*receive_callback)(int fd) = default_receive_callback;

/*
 * Hints for TCP
 */
int32_t nwc_hints_tcp_set(struct addrinfo *hints)
{
    int32_t retcode = -1;

    if (NULL == hints)
    {
    }
    else
    {
        memset(hints, 0, sizeof(*hints));
        hints->ai_family   = AF_UNSPEC;
        hints->ai_socktype = SOCK_STREAM;
        hints->ai_flags    = AI_PASSIVE;
        retcode = 0;
    }

    return retcode;
}

#define MAX_TCP_CONNECTIONS 32

/*
 * Hints for UDP listening (connectionless)
 */
int32_t nwc_hints_udp_set(struct addrinfo *hints)
{
    int32_t retcode = -1;

    if (NULL == hints)
    {
    }
    else
    {
        memset(hints, 0, sizeof(*hints));
#if 0
        hints->ai_family   = AF_UNSPEC; /* Can we get away with this? */
#else
        hints->ai_family   = AF_INET;
#endif
        hints->ai_socktype = SOCK_DGRAM;
        hints->ai_flags    = AI_PASSIVE;
        retcode = 0;
    }

    return retcode;
}

/*
 * Hints for UDP sending (connectionless)
 */
int32_t nwc_hints_udp_send_set(struct addrinfo *hints)
{
    int32_t retcode = -1;

    if (NULL == hints)
    {
    }
    else
    {
        memset(hints, 0, sizeof(*hints));
#if 0
        hints->ai_family   = AF_UNSPEC; /* Can we get away with this? */
#else
        hints->ai_family   = AF_INET;
#endif
        hints->ai_socktype = SOCK_DGRAM;
        retcode = 0;
    }

    return retcode;
}
/*
 * Suppress "already in use" message.
 */
void nwc_aiu_suppress(int socket)
{
    int yes = 1;
    setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
}

/*
 * Get a listener socket for TCP on the specified port with the specidied
 * backlog (maximum number of client connections).
 *
 * Returns 0 on success and non-zero on failure.
 */
int32_t nwc_listener_tcp_socket_get(const char *restrict port, int backlog, int *lfd)
{
    int32_t retcode = - 1;
    int32_t gai_retcode = - 1;
    struct addrinfo hints;
    struct addrinfo *ainf;
    struct addrinfo *pinf;
    int listener;

    if (NULL == port)
    {
        TDR_DIAG_ERROR("NULL pointer to port/service string.\n");
    }
    else if ((backlog < 1) || (backlog > MAX_TCP_CONNECTIONS))
    {
        TDR_DIAG_ERROR("Backlog (number of connections) out of range. Max: %d.\n", MAX_TCP_CONNECTIONS);
    }
    else if (NULL == lfd)
    {
        TDR_DIAG_ERROR("NULL pointer to listener fd.\n");
    }
    else
    {
        nwc_hints_tcp_set(&hints);

        if (0 != (gai_retcode = getaddrinfo(NULL, port, &hints, &ainf)))
        {
            TDR_DIAG_ERROR("getaddrinfo() failed: %s.\n", gai_strerror(gai_retcode));
        }
        else
        {
            /*
             * Iterate over the list returned by getaddrinfo() looking for a
             * listener to which to bind.
             */
            for (pinf = ainf; pinf != NULL; pinf = pinf->ai_next)
            {
                if (0 > (listener = socket(pinf->ai_family, pinf->ai_socktype, pinf->ai_protocol)))
                {
                }
                else
                {
                    nwc_aiu_suppress(listener); /* Suppress already in use message. */

                    if (0 > bind(listener, pinf->ai_addr, pinf->ai_addrlen))
                    {
                        close(listener);
                    }
                    else
                    {
                        /*
                         * We bound to the socket. Exit the loop.
                         */
                        break;
                    }
                }
            }

            if (NULL == pinf)
            {
                /*
                 * We fell off the end of the list so didn't bind to a socket.
                 */
                TDR_DIAG_ERROR("bind() failed.\n");
            }
            else
            {
                freeaddrinfo(ainf);

                /*
                 * Listen for incoming connections.
                 */
                if (-1 == listen(listener, backlog))
                {
                    TDR_DIAG_ERROR("listen() failed.\n");
                }
                else
                {
                    TDR_DIAG_INFO("Bound.\n");
                    *lfd = listener;
                    retcode = 0;
                }
            }
        }
    }

    return retcode;
}

/*
 * Get a listener socket for UDP on the specified port.
 *
 * Returns 0 on success and non-zero on failure.
 */
int32_t nwc_listener_udp_socket_get(const char *restrict port, int *udpfd)
{
    int32_t retcode = - 1;
    int32_t gai_retcode = - 1;
    struct addrinfo hints;
    struct addrinfo *ainf;
    struct addrinfo *pinf;
    int listener;

    if (NULL == port)
    {
        TDR_DIAG_ERROR("NULL pointer to port/service string.\n");
    }
    else if (NULL == udpfd)
    {
        TDR_DIAG_ERROR("NULL pointer to listener fd.\n");
    }
    else
    {
        nwc_hints_udp_set(&hints);

        if (0 != (gai_retcode = getaddrinfo(NULL, port, &hints, &ainf)))
        {
            TDR_DIAG_ERROR("getaddrinfo() failed: %s.\n", gai_strerror(gai_retcode));
        }
        else
        {
            /*
             * Iterate over the list returned by getaddrinfo() looking for a
             * listener to which to bind.
             */
            for (pinf = ainf; pinf != NULL; pinf = pinf->ai_next)
            {
                if (0 > (listener = socket(pinf->ai_family, pinf->ai_socktype, pinf->ai_protocol)))
                {
                }
                else
                {
                    nwc_aiu_suppress(listener); /* Suppress already in use message. */

                    if (0 > bind(listener, pinf->ai_addr, pinf->ai_addrlen))
                    {
                        close(listener);
                    }
                    else
                    {
                        /*
                         * We bound to the socket. Exit the loop.
                         */
                        break;
                    }
                }
            }

            if (NULL == pinf)
            {
                /*
                 * We fell off the end of the list so didn't bind to a socket.
                 */
                TDR_DIAG_ERROR("bind() failed.\n");
            }
            else
            {
                freeaddrinfo(ainf);
                TDR_DIAG_INFO("Bound.\n");
                *udpfd = listener;
                retcode = 0;
            }
        }
    }

    return retcode;
}

/*
 * Get a sender socket for UDP on the specified port.
 *
 * Returns 0 on success and non-zero on failure.
 */
int32_t nwc_sender_udp_socket_get(const char *server, const char *restrict port, int *udpfd, struct addrinfo **p)
{
    int32_t retcode = - 1;
    int32_t gai_retcode = - 1;
    struct addrinfo hints;
    struct addrinfo *ainf;
    struct addrinfo *pinf;
    int listener;

    if (NULL == port)
    {
        TDR_DIAG_ERROR("NULL pointer to port/service string.\n");
    }
    else if (NULL == udpfd)
    {
        TDR_DIAG_ERROR("NULL pointer to listener fd.\n");
    }
    else if (NULL == p)
    {
        TDR_DIAG_ERROR("NULL pointer to struct addrinfo.\n");
    }
    else
    {
        nwc_hints_udp_send_set(&hints);

        if (0 != (gai_retcode = getaddrinfo(server, port, &hints, &ainf)))
        {
            TDR_DIAG_ERROR("getaddrinfo() failed: %s.\n", gai_strerror(gai_retcode));
        }
        else
        {
            /*
             * Iterate over the list returned by getaddrinfo() looking for a
             * listener to which to bind.
             */
            for (pinf = ainf; pinf != NULL; pinf = pinf->ai_next)
            {
                if (0 > (listener = socket(pinf->ai_family, pinf->ai_socktype, pinf->ai_protocol)))
                {
                }
                else
                {
                    /*
                     * We bound to the socket. Exit the loop.
                     */
                    break;
                }
            }

            if (NULL == pinf)
            {
                /*
                 * We fell off the end of the list so didn't bind to a socket.
                 */
                TDR_DIAG_ERROR("bind() failed.\n");
            }
            else
            {
                freeaddrinfo(ainf);
                TDR_DIAG_INFO("Bound.\n");
                *udpfd = listener;
                *p = pinf;
                retcode = 0;
            }
        }
    }

    return retcode;
}

void *nwc_in_addr_get(struct sockaddr *sa)
{
    void *in_addr;

    if (AF_INET == sa->sa_family)
    {
        in_addr = &(((struct sockaddr_in*)sa)->sin_addr);
    }
    else
    {
        in_addr = &(((struct sockaddr_in6*)sa)->sin6_addr);
    }

    return in_addr;
}

/*
 * Main polling loop for TCP sockets.
 */
#define POLL_TIMEOUT_BLOCK  -1
#define RECEIVE_BUF_SIZE     256
int32_t nwc_tcp_polling_loop(pfdtable_t *pfdtable)
{
    int32_t   retcode = -1;
    int       poll_count;
    int32_t   index;
    struct    pollfd *fds;
    int32_t   nfds;
    int       lfd;
    int       newfd;
    struct    sockaddr_storage remoteaddr;
    socklen_t addrlen;
    char      remoteIP[INET6_ADDRSTRLEN];
    int32_t   nbytes;
    int       sender_fd;
    char      buf[RECEIVE_BUF_SIZE];
    int32_t   jindex;
    int       dest_fd;

    if (NULL == pfdtable)
    {
        TDR_DIAG_ERROR("NULL pointer to pfdtable.\n");
    }
    else
    {
        fds  = pfdtable_get_fds(pfdtable);
        nfds = pfdtable_get_used(pfdtable);
        lfd  = pfdtable_get_listener(pfdtable);

        TDR_DIAG_INFO("Number of file descriptors: %d.\n", nfds);
        TDR_DIAG_INFO("Listener file descriptor: %d.\n", lfd);

        poll_count = poll(fds, nfds, POLL_TIMEOUT_BLOCK);

        if (-1 == poll_count)
        {
            TDR_DIAG_ERROR("poll() failed.\n");
        }
        else
        {
            for (index = 0; index < nfds; index++)
            {
                if ( ! (fds[index].revents & POLLIN))
                {
                    /*
                     * Nothing to do. Skip.
                     */
                }
                else if (lfd == fds[index].fd)
                {
                    /*
                     * This is the listener.
                     */
                    TDR_DIAG_INFO("Listener has data.\n");
                    addrlen = sizeof(remoteaddr);
                    newfd   = accept(lfd, (struct sockaddr *)&remoteaddr, &addrlen);
                    if (-1 == newfd)
                    {
                        TDR_DIAG_ERROR("accept() failed.\n");
                    }
                    else if (0 != pfdtable_add_fd(pfdtable, newfd))
                    {
                        TDR_DIAG_ERROR("Failed to add client file descriptor to table.\n");
                    }
                    else
                    {
                        TDR_DIAG_INFO("New connection from %s on socket fd %d.\n",
                                       inet_ntop(remoteaddr.ss_family,
                                                 nwc_in_addr_get((struct sockaddr*)&remoteaddr),
                                                 remoteIP,
                                                 INET6_ADDRSTRLEN),
                                       newfd);
                    }
                }
                else
                {
                    /*
                     * Ordinary client.
                     */
                    TDR_DIAG_INFO("Client.\n");
                    nbytes = recv(fds[index].fd, buf, sizeof(buf), 0);
                    sender_fd = fds[index].fd;

                    if (nbytes <= 0)
                    {
                        // Got error or connection closed by client
                        if (0 == nbytes)
                        {
                            /*
                             * The connection was closed.
                             */
                            TDR_DIAG_INFO("Socket fd %d hung up\n", sender_fd);
                        }
                        else
                        {
                            TDR_DIAG_ERROR("recv() error.\n");
                        }

                        close(fds[index].fd);
                        pfdtable_del_fd(pfdtable, index);
                    }
                    else
                    {
                        /*
                         * We got some valid data from a client.
                         */

                        for(jindex = 0; jindex < nfds; jindex++)
                        {
                            /*
                             * Send the data to everyone except the listener and
                             * ourselves.
                             */
                            dest_fd = fds[jindex].fd;

                            if (lfd == dest_fd)
                            {
                                /* skip */
                            }
                            else if (dest_fd == sender_fd)
                            {
                                /* skip */
                            }
                            else if (-1 == send(dest_fd, buf, nbytes, 0))
                            {
                                TDR_DIAG_ERROR("send() failed.\n");
                            }
                        }
                    }
                }
            }
        }
    }

    return retcode;
}

/*
 * If we receive data on a socket, call a callback function.
 */
int32_t nwc_tcp_callback_loop(pfdtable_t *pfdtable)
{
    int32_t   retcode = -1;
    int       poll_count;
    int32_t   index;
    struct    pollfd *fds;
    int32_t   nfds;
    int       lfd;
    int       newfd;
    struct    sockaddr_storage remoteaddr;
    socklen_t addrlen;
    char      remoteIP[INET6_ADDRSTRLEN];

    if (NULL == pfdtable)
    {
        TDR_DIAG_ERROR("NULL pointer to pfdtable.\n");
    }
    else
    {
        fds  = pfdtable_get_fds(pfdtable);
        nfds = pfdtable_get_used(pfdtable);
        lfd  = pfdtable_get_listener(pfdtable);

        TDR_DIAG_INFO("Number of file descriptors: %d.\n", nfds);
        TDR_DIAG_INFO("Listener file descriptor: %d.\n", lfd);

        poll_count = poll(fds, nfds, POLL_TIMEOUT_BLOCK);

        if (-1 == poll_count)
        {
            TDR_DIAG_ERROR("poll() failed.\n");
        }
        else
        {
            for (index = 0; index < nfds; index++)
            {
                if ( ! (fds[index].revents & POLLIN))
                {
                    /*
                     * Nothing to do. Skip.
                     */
                }
                else if (lfd == fds[index].fd)
                {
                    /*
                     * This is the listener.
                     */
                    TDR_DIAG_INFO("Listener has data.\n");
                    addrlen = sizeof(remoteaddr);
                    newfd   = accept(lfd, (struct sockaddr *)&remoteaddr, &addrlen);
                    if (-1 == newfd)
                    {
                        TDR_DIAG_ERROR("accept() failed.\n");
                    }
                    else if (0 != pfdtable_add_fd(pfdtable, newfd))
                    {
                        TDR_DIAG_ERROR("Failed to add client file descriptor to table.\n");
                    }
                    else
                    {
                        TDR_DIAG_INFO("New connection from %s on socket fd %d.\n",
                                       inet_ntop(remoteaddr.ss_family,
                                                 nwc_in_addr_get((struct sockaddr*)&remoteaddr),
                                                 remoteIP,
                                                 INET6_ADDRSTRLEN),
                                       newfd);
                    }
                }
                else
                {
                    /*
                     * Ordinary client.
                     */
                    TDR_DIAG_INFO("Client.\n");
                    receive_callback(fds[index].fd);
                }
            }
        }
    }

    return retcode;
}

#define COLUMN_MAX  16
void hexdump(uint8_t *buffer, size_t size)
{
    int32_t index;
    int32_t column = 0;

    for (index = 0; index < size; index++)
    {
        printf("%02hhx ", buffer[index]);
        column++;
        if (COLUMN_MAX == column)
        {
            column = 0;
            printf("\n");
        }
    }

    if (0 != column)
    {
        printf("\n");
    }
}

#define UDP_PACKET_MAX  65536
int32_t nwc_udp_polling_loop(int udpfd)
{
    int32_t retcode = -1;
    struct sockaddr_storage remote_addr;
    socklen_t addr_len;
    int32_t nreceived;
    uint8_t buffer[UDP_PACKET_MAX];
    char str[INET6_ADDRSTRLEN];


    addr_len = sizeof(remote_addr);
    if (-1 == (nreceived = recvfrom(udpfd, buffer, sizeof(buffer) - 1, 0,
                                    (struct sockaddr *)&remote_addr,
                                    &addr_len)))
    {
    }
    else
    {
        TDR_DIAG_INFO("Received packet from %s\n", inet_ntop(remote_addr.ss_family,
                                                             nwc_in_addr_get((struct sockaddr *)&remote_addr),
                                                             str, sizeof(str)));
        TDR_DIAG_INFO("Packet length: %d\n", nreceived);
        hexdump(buffer, nreceived);
        retcode = 0;
    }

    return retcode;
}

typedef struct
{
    const char *port;
    int udpfd;
    buffer_t *buffer;
}
udp_thread_args_t;

#define UDP_CALLBACK_SLEEP_PERIOD_US    (1000)

int32_t udp_callback_server_receive(int udpfd, buffer_t *buffer)
{
    int32_t retcode = -1;
    struct sockaddr_storage remote_addr;
    socklen_t addr_len;
    int32_t nreceived;
    char str[INET6_ADDRSTRLEN];
    uint8_t internal[UDPIPV4_PAYLOAD_MAX];

    TDR_DIAG_INFO("File descriptor: %d\n", udpfd);

    addr_len = sizeof(remote_addr);
    if (-1 == (nreceived = recvfrom(udpfd, internal, sizeof(internal) - 1, 0,
                                    (struct sockaddr *)&remote_addr,
                                    &addr_len)))
    {
    }
    else
    {
        TDR_DIAG_INFO("Received packet from %s\n", inet_ntop(remote_addr.ss_family,
                                                             nwc_in_addr_get((struct sockaddr *)&remote_addr),
                                                             str, sizeof(str)));
        TDR_DIAG_INFO("Packet length: %d\n", nreceived);
        if (0 != buffer_write_data(buffer, internal, nreceived))
        {
            TDR_DIAG_ERROR("Failed to transfer packet data to buffer.\n");
        }
        else
        {
            TDR_DIAG_INFO("Calling notification callback.\n");
            if (0 != buffer->notify(buffer))
            {
                TDR_DIAG_INFO("Notification callback failed.\n");
            }
            else
            {
                retcode = 0;
            }
        }
    }

    return retcode;
}

void *udp_callback_server_thread(void *args)
{
    udp_thread_args_t *udp_thread_args;

    udp_thread_args = (udp_thread_args_t *)args;
    TDR_DIAG_INFO("Starting on port: %s\n", udp_thread_args->port);
    TDR_DIAG_INFO("Starting on fd: %d\n", udp_thread_args->udpfd);
    TDR_DIAG_INFO("buffer: %p\n", udp_thread_args->buffer);

    for (;;)
    {
        if (0 != udp_callback_server_receive(udp_thread_args->udpfd, udp_thread_args->buffer))
        {
            break;
        }
#if 0
        /*
         * We don't need to do this if we're using a blocking receieve.
         */
        TDR_DIAG_INFO("Sleeping.\n");
        usleep(UDP_CALLBACK_SLEEP_PERIOD_US);
#endif
    }

    pthread_exit(NULL);
}
    udp_thread_args_t  udp_thread_args;

int32_t udp_callback_server_thread_loop(const char *port, int udpfd, int32_t (*notify)(struct buffer_t *buffer))
{
    int32_t            retcode = -1;
    pthread_t          udp_thread;
    pthread_attr_t     udp_threadattr;
    buffer_t          *buffer;

    udp_thread_args.port  = port;
    udp_thread_args.udpfd = udpfd;
    pthread_attr_init(&udp_threadattr);

    TDR_DIAG_INFO("File descriptor: %d\n", udpfd);

    if (NULL == (buffer = buffer_create(UDPIPV4_PAYLOAD_MAX)))
    {
        TDR_DIAG_ERROR("Failed to create receive buffer.\n");
    }
    else
    {
        buffer->notify = notify;
        udp_thread_args.buffer = buffer;
        TDR_DIAG_INFO("Port: %s\n", udp_thread_args.port);
        TDR_DIAG_INFO("File descriptor: %d\n", udp_thread_args.udpfd);
        TDR_DIAG_INFO("Buffer: %p\n", udp_thread_args.buffer);
        if (0 != pthread_create(&udp_thread, &udp_threadattr, udp_callback_server_thread, (void *)&udp_thread_args))
        {
            TDR_DIAG_ERROR("Failed to create thread.\n");
        }
        else
        {
            TDR_DIAG_INFO("Thread created.\n");
            retcode = 0;
        }
    }

    pthread_attr_destroy(&udp_threadattr);

    return retcode;
}

int32_t nwc_udp_callback_server(const char *restrict port, int32_t (*notify)(struct buffer_t *buffer))
{
    int32_t retcode = -1;
    int udpfd;

    if (0 != nwc_listener_udp_socket_get(port, &udpfd))
    {
    }
    else if (0 != udp_callback_server_thread_loop(port, udpfd, notify))
    {
        TDR_DIAG_INFO("Failed to start server thread.\n");
    }
    else
    {
        retcode = 0;
    }

    return retcode;
}
