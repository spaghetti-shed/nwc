/******************************************************************************
 * pfdtable_test.c: Table of polling file descriptors. (test)
 *
 * Sat Nov  4 16:14:36 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#include <poll.h>

#include "pfdtable.h"
#include "cutl.h"
#include "mal.h"

void test_pfdtable_allocation(void);
void test_pfdtable_accessors(void);
void test_pfdtable_add_struct_fd(void);
void test_pfdtable_del_fd(void);

cutl_test_case_t tests[] =
{
    { "pfdtable_allocation",    test_pfdtable_allocation    },
    { "pfdtable_accessors",     test_pfdtable_accessors     },
    { "pfdtable_add_struct_fd", test_pfdtable_add_struct_fd },
    { "pfdtable_del_fd",        test_pfdtable_del_fd        },
};

void pfdtable_test_case_setup(void);
void pfdtable_test_case_teardown(void);

#define NO_OF_OBJECTS   1024

void pfdtable_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
    cutl_stdlib_test_stubs_reset();
}

void pfdtable_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();
    if (nspare > 0)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }
}

int main(int argc, char *argv[])
{
    printf("pfdtable test.\n");

    cutl_set_test_lib("pfdtable", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(pfdtable_test_case_setup);
    cutl_set_cutl_test_teardown(pfdtable_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

#define NO_OF_FDS   5

void test_pfdtable_allocation(void)
{
    pfdtable_t *pfdtable = NULL;

    pfdtable = pfdtable_create(NO_OF_FDS);
    TEST_PTR_NOT_NULL("pfdtable_create()", pfdtable);
    TEST_INT32_EQUAL("MAL_IS_VALID", 1, MAL_IS_VALID(pfdtable));

    TEST_PTR_NOT_NULL("fds", pfdtable_get_fds(pfdtable));
    TEST_SIZE_T_EQUAL("size", NO_OF_FDS, pfdtable_get_size(pfdtable));
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(pfdtable));
    TEST_INT32_EQUAL("listener", 0, pfdtable_get_listener(pfdtable));

    pfdtable_destroy(&pfdtable);
    TEST_PTR_NULL("pfdtable_destroy()", pfdtable);
    TEST_INT32_EQUAL("MAL_IS_VALID", 0, MAL_IS_VALID(pfdtable));
}


void test_pfdtable_accessors(void)
{
    pfdtable_t pfdtable;

    pfdtable_set_fds(&pfdtable, 0);
    TEST_PTR_NULL("fds", pfdtable_get_fds(&pfdtable));

    pfdtable_set_size(&pfdtable, 0);
    TEST_SIZE_T_EQUAL("size", 0, pfdtable_get_size(&pfdtable));

    pfdtable_set_used(&pfdtable, 0);
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(&pfdtable));

    pfdtable_set_listener(&pfdtable, 0);
    TEST_INT32_EQUAL("listener", 0, pfdtable_get_listener(&pfdtable));
}

/*
 * Add struct pollfds to the table until we fill it up.
 */
void test_pfdtable_add_struct_fd(void)
{
    pfdtable_t *pfdtable = NULL;
    int32_t retcode;

    struct pollfd test_fds[] =
    {
        { 3, 0, 0 },
        { 4, 0, 0 },
        { 5, 0, 0 },
        { 6, 0, 0 },
        { 7, 0, 0 },
        { 8, 0, 0 },
    };

    struct pollfd output_fd = { 0 };

    pfdtable = pfdtable_create(NO_OF_FDS);
    TEST_PTR_NOT_NULL("pfdtable_create()", pfdtable);
    TEST_INT32_EQUAL("MAL_IS_VALID", 1, MAL_IS_VALID(pfdtable));

    TEST_PTR_NOT_NULL("fds", pfdtable_get_fds(pfdtable));
    TEST_SIZE_T_EQUAL("size", NO_OF_FDS, pfdtable_get_size(pfdtable));
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(pfdtable));
    TEST_INT32_EQUAL("listener", 0, pfdtable_get_listener(pfdtable));

    /*
     * First.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[0]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 1, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 0, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[0].fd);

    /*
     * Second.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[1]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 2, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 1, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[1].fd);

    /*
     * Third.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[2]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 3, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 2, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[2].fd);

    /*
     * Fourth.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[3]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 4, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 3, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[3].fd);

    /*
     * Fifth.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[4]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 5, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 4, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[4].fd);

    /*
     * Table full. Attempting to add another fails.
     */
    retcode = pfdtable_add_struct_fd(pfdtable, test_fds[5]);
    TEST_INT32_EQUAL("pfdtable_add_struct_fd", -1, retcode);

    pfdtable_destroy(&pfdtable);
    TEST_PTR_NULL("pfdtable_destroy()", pfdtable);
    TEST_INT32_EQUAL("MAL_IS_VALID", 0, MAL_IS_VALID(pfdtable));
}

/*
 * Delete struct pollfds from the table until it's empty.
 */
void test_pfdtable_del_fd(void)
{
    pfdtable_t *pfdtable = NULL;
    int32_t retcode;
    int32_t index;

    struct pollfd test_fds[] =
    {
        { 3, 0, 0 },
        { 4, 0, 0 },
        { 5, 0, 0 },
        { 6, 0, 0 },
        { 7, 0, 0 },
    };

    struct pollfd output_fd = { 0 };

    pfdtable = pfdtable_create(NO_OF_ELEMENTS(test_fds));
    TEST_PTR_NOT_NULL("pfdtable_create()", pfdtable);

    for (index = 0; index < NO_OF_ELEMENTS(test_fds); index++)
    {
        retcode = pfdtable_add_struct_fd(pfdtable, test_fds[index]);
        TEST_INT32_EQUAL("pfdtable_add_struct_fd", 0, retcode);
    }
    TEST_INT32_EQUAL("used", NO_OF_ELEMENTS(test_fds), pfdtable_get_used(pfdtable));

    /*
     * Delete the first.
     * Show that it got replaced with the last from the table,
     */
    retcode = pfdtable_del_fd(pfdtable, 0);
    TEST_INT32_EQUAL("pfdtable_del_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 4, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 0, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[4].fd);

    /*
     * Delete the last.
     */
    retcode = pfdtable_del_fd(pfdtable, 3);
    TEST_INT32_EQUAL("pfdtable_del_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 3, pfdtable_get_used(pfdtable));

    /*
     * Try to delete one beyond the end of the table.
     */
    retcode = pfdtable_del_fd(pfdtable, 3);
    TEST_INT32_EQUAL("pfdtable_del_fd", -1, retcode);
    TEST_INT32_EQUAL("used", 3, pfdtable_get_used(pfdtable));

    /*
     * Delete the first.
     * Show that it got replaced with the last from the table,
     */
    retcode = pfdtable_del_fd(pfdtable, 0);
    TEST_INT32_EQUAL("pfdtable_del_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 2, pfdtable_get_used(pfdtable));

    retcode = pfdtable_get_struct_fd(pfdtable, 0, &output_fd);
    TEST_INT32_EQUAL("pfdtable_get_struct_fd", 0, retcode);
    TEST_INT32_EQUAL("fd", output_fd.fd, test_fds[2].fd);

    /*
     * Delete the last.
     */
    retcode = pfdtable_del_fd(pfdtable, 1);
    TEST_INT32_EQUAL("pfdtable_del_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 1, pfdtable_get_used(pfdtable));

    /*
     * Delete the first/last.
     */
    retcode = pfdtable_del_fd(pfdtable, 0);
    TEST_INT32_EQUAL("pfdtable_del_fd", 0, retcode);
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(pfdtable));

    /*
     * Try to delete the first/last again.
     */
    retcode = pfdtable_del_fd(pfdtable, 0);
    TEST_INT32_EQUAL("pfdtable_del_fd", -1, retcode);
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(pfdtable));

    /*
     * Try to delete from a negative index.
     */
    retcode = pfdtable_del_fd(pfdtable, -1);
    TEST_INT32_EQUAL("pfdtable_del_fd", -1, retcode);
    TEST_INT32_EQUAL("used", 0, pfdtable_get_used(pfdtable));

    pfdtable_destroy(&pfdtable);
    TEST_PTR_NULL("pfdtable_destroy()", pfdtable);
    TEST_INT32_EQUAL("MAL_IS_VALID", 0, MAL_IS_VALID(pfdtable));
}


