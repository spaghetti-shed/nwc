/******************************************************************************
 * pfdtable.h: Table of polling file descriptors. (header)
 * Sat Nov  4 16:14:36 GMT 2023
 *
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __PFDTABLE_H__
#define __PFDTABLE_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
    struct pollfd *fds;
    size_t         size;
    int32_t        used;
    int32_t        listener;
}
pfdtable_t;

pfdtable_t *pfdtable_create(size_t nfds);
void        pfdtable_destroy(pfdtable_t **pfdtable);

struct pollfd *pfdtable_get_fds(pfdtable_t *pfdtable);
void           pfdtable_set_fds(pfdtable_t *pfdtable, struct pollfd * fds);
size_t         pfdtable_get_size(pfdtable_t *pfdtable);
void           pfdtable_set_size(pfdtable_t *pfdtable, size_t size);
int32_t        pfdtable_get_used(pfdtable_t *pfdtable);
void           pfdtable_set_used(pfdtable_t *pfdtable, int32_t used);
int32_t        pfdtable_get_listener(pfdtable_t *pfdtable);
void           pfdtable_set_listener(pfdtable_t *pfdtable, int32_t listener);

int32_t pfdtable_add_struct_fd(pfdtable_t *pfdtable, struct pollfd pfd);
int32_t pfdtable_get_struct_fd(pfdtable_t *pfdtable, int32_t index, struct pollfd *pfd);
int32_t pfdtable_del_fd(pfdtable_t *pfdtable, int32_t index);

int32_t pfdtable_add_fd(pfdtable_t *pfdtable, int pfd);
int32_t pfdtable_get_fd(pfdtable_t *pfdtable, int32_t index, int *pfd);

#ifdef __cplusplus
}
#endif

#endif /* __PFDTABLE_H__ */

