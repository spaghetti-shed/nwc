/******************************************************************************
 * pfdtable.c: Table of polling file descriptors.
 *
 * Sat Nov  4 16:14:36 GMT 2023
 * Copyright (C) 2023 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of nwc.
 *
 * nwc is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * nwc is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with nwc; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2023-11-04 Initial creation.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#include <poll.h>

#include "timber.h"
#include "mal.h"
#include "pfdtable.h"

pfdtable_t *pfdtable_allocate(size_t nfds)
{
    pfdtable_t *pfdtable;

    if (NULL == (pfdtable = MAL_MALLOC(sizeof(pfdtable_t))))
    {
    }
    else if (NULL == (pfdtable->fds = MAL_MALLOC(sizeof(struct pollfd) * nfds)))
    {
        MAL_FREE(pfdtable);
        pfdtable = NULL;
    }

    return pfdtable;
}

void pfdtable_init(pfdtable_t *pfdtable, size_t nfds)
{
    pfdtable_set_size(pfdtable, nfds);
    pfdtable_set_used(pfdtable, 0);
    pfdtable_set_listener(pfdtable, 0);
}

pfdtable_t *pfdtable_create(size_t nfds)
{
    pfdtable_t *pfdtable;

    pfdtable = pfdtable_allocate(nfds);
    if(NULL == pfdtable)
    {
        TDR_DIAG_ERROR("pfdtable_allocate() failed.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        pfdtable_init(pfdtable, nfds);
    }

    return pfdtable;
}

void pfdtable_destroy(pfdtable_t **pfdtable)
{
    if(NULL == pfdtable)
    {
        TDR_DIAG_ERROR("pfdtable_destroy(): NULL pointer received.\n");
    }
    else
    {
        if (NULL == (*pfdtable)->fds)
        {
            /* This should be impossible. */
        }
        else
        {
            MAL_FREE((*pfdtable)->fds);
        }
        MAL_FREE(*pfdtable);
        *pfdtable = NULL;
    }
}

struct pollfd * pfdtable_get_fds(pfdtable_t *pfdtable)
{
    return pfdtable->fds;
}


void pfdtable_set_fds(pfdtable_t *pfdtable, struct pollfd * fds)
{
    pfdtable->fds = fds;
}

size_t pfdtable_get_size(pfdtable_t *pfdtable)
{
    return pfdtable->size;
}


void pfdtable_set_size(pfdtable_t *pfdtable, size_t size)
{
    pfdtable->size = size;
}

int32_t pfdtable_get_used(pfdtable_t *pfdtable)
{
    return pfdtable->used;
}


void pfdtable_set_used(pfdtable_t *pfdtable, int32_t used)
{
    pfdtable->used = used;
}

int32_t pfdtable_get_listener(pfdtable_t *pfdtable)
{
    return pfdtable->listener;
}


void pfdtable_set_listener(pfdtable_t *pfdtable, int32_t listener)
{
    pfdtable->listener = listener;
}

int32_t pfdtable_is_valid(pfdtable_t *pfdtable)
{
    int32_t retcode = -1;

    if (NULL == pfdtable)
    {
        TDR_DIAG_ERROR("NULL pointer to pfdtable.\n");
    }
    else if (NULL == pfdtable->fds)
    {
        TDR_DIAG_ERROR("NULL pointer to fd array.\n");
    }
    else if (0 == pfdtable->size)
    {
        TDR_DIAG_ERROR("Table has zero size.\n");
    }
    else
    {
        retcode = 0;
    }

    return retcode;
}

int32_t pfdtable_add_struct_fd(pfdtable_t *pfdtable, struct pollfd pfd)
{
    int32_t retcode = -1;

    if (-1 == pfdtable_is_valid(pfdtable))
    {
    }
    else if (pfdtable->used == pfdtable->size)
    {
    }
    else
    {
        pfdtable->fds[pfdtable->used] = pfd;
        pfdtable->used++;
        retcode = 0;
    }

    return retcode;
}

int32_t pfdtable_get_struct_fd(pfdtable_t *pfdtable, int32_t index, struct pollfd *pfd)
{
    int32_t retcode = -1;

    if (-1 == pfdtable_is_valid(pfdtable))
    {
    }
    else if (NULL == pfd)
    {
    }
    else if ((index < 0) || (index > ((pfdtable->used - 1))))
    {
        TDR_DIAG_ERROR("Index %d out of range (%d).\n", index, (pfdtable->used - 1));
    }
    else
    {
        *pfd = pfdtable->fds[index];
        retcode = 0;
    }

    return retcode;
}

int32_t pfdtable_del_fd(pfdtable_t *pfdtable, int32_t index)
{
    int32_t retcode = -1;

    if (-1 == pfdtable_is_valid(pfdtable))
    {
    }
    else if ((index < 0) || (index > ((pfdtable->used - 1))))
    {
        TDR_DIAG_ERROR("Index %d out of range (%d).\n", index, (pfdtable->used - 1));
    }
    else
    {
        pfdtable->fds[index] = pfdtable->fds[(pfdtable->used - 1)];
        pfdtable->used--;
        retcode = 0;
    }

    return retcode;
}

int32_t pfdtable_add_fd(pfdtable_t *pfdtable, int fd)
{
    struct pollfd pfd;

    pfd.fd     = fd;
    pfd.events = POLLIN;

    return pfdtable_add_struct_fd(pfdtable, pfd);
}

int32_t pfdtable_get_fd(pfdtable_t *pfdtable, int32_t index, int *fd)
{
    int32_t retcode = -1;
    struct pollfd pfd;

    if (NULL == fd)
    {
        TDR_DIAG_ERROR("NULL pointer to fd.\n");
    }
    else if (0 != (retcode = pfdtable_get_struct_fd(pfdtable, index, &pfd)))
    {
    }
    else
    {
        *fd = pfd.fd;
    }

    return retcode;
}

